library(shiny)
library(data.table)
library(magrittr)
library(DT)
library(readxl)
# library(reactlog)

options(shiny.maxRequestSize = 100*1024^2)
# options(shiny.reactlog=TRUE) 
# reactlog_enable()

shinyServer(function(input, output) {
    
    hideTab("main", "File 1")
    hideTab("main", "File 2")

    values <- reactiveValues(
        data1 = data.table(),
        data2 = data.table(),
        excelFlag1 = FALSE,
        excelFlag2 = FALSE
    )
    
    output$contents1 <- renderDT(values$data1)
    # output$contents2 <- renderDT(values$data2)
    # outputOptions(output, "contents", suspendWhenHidden = FALSE)
    # outputOptions(output, "contents2", suspendWhenHidden = FALSE)
    
    output$fileInputPanel1 <-
        renderUI(file_input_panel(title = "Upload sample metadata", number = 1))
    output$fileInputPanel2 <-
        renderUI(file_input_panel(title = "Upload another file with sample IDs", number = 2))
    output$excelFlag1 <- reactive(values$excelFlag1)
    output$excelFlag2 <- reactive(values$excelFlag2)
    
    output$selectSheet1 <- renderUI({
        selectInput(
            inputId = "sheetSelection1",
            label = "Select a sheet.",
            choices = c("Select a sheet" = "")
        )
    })
    output$selectSheet2 <- renderUI({
        selectInput(
            inputId = "sheetSelection2",
            label = "Select a sheet.",
            choices = c("Select a sheet" = "")
        )
    })
    outputOptions(output, "excelFlag1", suspendWhenHidden = FALSE)
    outputOptions(output, "selectSheet1", suspendWhenHidden = FALSE)
    outputOptions(output, "excelFlag2", suspendWhenHidden = FALSE)
    outputOptions(output, "selectSheet2", suspendWhenHidden = FALSE)
    outputOptions(output, "fileInputPanel1", suspendWhenHidden = FALSE)
    outputOptions(output, "fileInputPanel2", suspendWhenHidden = FALSE)
    
    observeEvent(eventExpr = input$fileInput1,
                 handlerExpr = {
                     # message("file input!")
                     values$excelFlag1 <-
                         file_type(input$fileInput1$name) == 2
                     message("Excel flag 1 ", values$excelFlag1)
                     if (values$excelFlag1) {
                         sheets <- excel_sheets(input$fileInput1$datapath)
                         message("Sheets: ", paste0(sheets, collapse="; "))
                         # message(paste(sheets, sep=";"))
                         updateSelectInput(
                             inputId = "sheetSelection1",
                             choices = sheets,
                             selected = ifelse("List of Samples" %in% sheets,
                                               "List of Samples",
                                               first(sheets))
                         )
                         message("sheetSelection1")
                         updateNumericInput(inputId = "headerRow1",
                                            value = 9)
                         message("headerRow1")
                     } else {
                         updateNumericInput(inputId = "headerRow1",
                                            value = 1)
                     }
                 })
    
    observeEvent(eventExpr = input$fileInput2,
                 handlerExpr = {
                     # message("file input!")
                     values$excelFlag2 <-
                         file_type(input$fileInput2$name) == 2
                     
                     message("Excel flag 2 ", values$excelFlag2)
                     if (values$excelFlag2) {
                         sheets <- excel_sheets(input$fileInput2$datapath)
                         # message(paste(sheets, sep=";"))
                         updateSelectInput(
                             inputId = "sheetSelection2",
                             choices = sheets,
                             selected = ifelse("List of Samples" %in% sheets,
                                               "List of Samples",
                                               NULL)
                         )
                         updateNumericInput(inputId = "headerRow2",
                                            value = 9)
                     } else {
                         updateNumericInput(inputId = "headerRow2",
                                            value = 1)
                     }
                 })
    
    observeEvent(eventExpr = input$viewData1,
                 handlerExpr =  {
                     req(input$fileInput1)
                     message(input$fileInput1)
                     if (values$excelFlag1) {
                         req(input$sheetSelection1)
                         values$data1 <-
                             read_samples_excel(input$fileInput1$datapath,
                                                input$sheetSelection1,
                                                input$headerRow1)
                     } else {
                         values$data1 <- read_samples_delim(input$fileInput1$datapath,
                                                            input$headerRow1)
                     }
                     showTab("main", target = "File 1", select = TRUE)
                     updateDisplayTable()
                     updateSelectInput(inputId = "parentColumnSelection",
                                       choices = c("Select column" = "", names(values$data1)))
                     updateSelectInput(inputId = "replicateColumnSelection",
                                       choices = c("Select column" = "", names(values$data1)))
                 })
    
    observeEvent(eventExpr = input$viewData2,
                 handlerExpr =  {
                     req(input$fileInput2)
                     message(input$fileInput2)
                     if (values$excelFlag2) {
                         req(input$sheetSelection2)
                         values$data2 <-
                             read_samples_excel(input$fileInput2$datapath,
                                                input$sheetSelection2,
                                                input$headerRow2)
                     } else {
                         values$data2 <- read_samples_delim(input$fileInput2$datapath,
                                                            input$headerRow2)
                     }
                     
                     showTab("main", target = "File 2", select = TRUE)
                     output$contents2 <- renderDT(
                         expr = values$data2,
                         options = list(
                             scrollX = TRUE,
                             pageLength = 10,
                             scrollCollapse = TRUE
                         )
                     )
                     updateSelectInput(inputId = "file1CompareColumn",
                                       choices = names(values$data1))
                     updateSelectInput(inputId = "file2CompareColumn",
                                       choices = names(values$data2))
                     
                 })
    
    observeEvent(eventExpr = input$doComparison,
                 handlerExpr = {
                     req(input$file1CompareColumn)
                     req(input$file2CompareColumn)
                     col1 <-
                         values$data1[[input$file1CompareColumn]] %>% sort()
                     col2 <-
                         values$data2[[input$file2CompareColumn]] %>% sort()
                     
                     sets_are_equal <- function(a, b) {
                         if (length(a) != length(b))
                             FALSE
                         else
                             all(a == b)
                     }
                     
                     showModal(tags$div(if (sets_are_equal(col1, col2)) {
                         modalDialog(
                             title = "Success",
                             tags$div(
                                 style = "background-color: LightGreen",
                                 "Both columns match.",
                                 icon("check-circle")
                             ),
                             easyClose = TRUE, 
                             fade = FALSE,
                             size = "s"
                         )
                     } else {
                         modalDialog(
                             title = "Failure",
                             tags$div(
                                 style = "background-color: lightsalmon",
                                 "The selected columns do not match.",
                                 icon("times-circle")
                             ),
                             easyClose = TRUE,
                             fade = FALSE,
                             size = "s"
                         )
                     }))
                 })
    
    updateDisplayTable <- function() {
        output$contents <<- renderDT(
            expr = copy(values$data1),
            options = list(
                scrollX = TRUE,
                pageLength = 10,
                scrollCollapse = TRUE
            )
        )
    }
    
    output$selectParentColumn <- renderUI({
        selectInput(
            "parentColumnSelection",
            label = "Pedigree column:",
            choices = c("No sample file uploaded" = "")
        )
    })
    outputOptions(output, "selectParentColumn", suspendWhenHidden = FALSE)
    output$selectReplicateColumn <- renderUI({
        selectInput(
            "replicateColumnSelection",
            label = "Replicate column:",
            choices = c("No sample file uploaded" = "")
        )
    })
    outputOptions(output, "selectReplicateColumn", suspendWhenHidden = FALSE)
    
    output$downloadTXT <-
        downloadDelim(
            filename = input$fileInput1$name,
            # data = values$display_data,
            data = values$data1,
            csv = FALSE
        )
    output$downloadCSV <-
        downloadDelim(
            filename = input$fileInput1$name,
            # data = values$display_data,
            data = values$data1,
            csv = TRUE
        )
    
    observeEvent(input$applyGermplasmStrip,
                 {
                     req(input$fileInput1,
                         input$replicateColumnSelection)
                     pattern <- paste0("(", input$germplasmStripValues, ")$")
                     # values$display_data[, "Germplasm Name" := sub(pattern, "", get(input$germplasmColumn))]
                     values$data1[, "Germplasm Name" := sub(pattern, "", get(input$replicateColumnSelection))]
                     updateDisplayTable()
                     updateTabsetPanel(inputId = "main", selected = "File 1")
                 })
    
    observeEvent(input$applyPedigreeSplit,
                 {
                     req(input$fileInput1,
                         input$parentColumnSelection)
                     pattern <-
                         paste0("(", input$parentSplitValues, ")")
                     message("pattern: ", pattern)
                     message("parent column: ", input$parentColumnSelection)
                     parents <- values$data1[, tstrsplit(get(input$parentColumnSelection), pattern)]
                     parents[is.na(V2), V1 := NA]
                     values$data1[, c("Parent 1", "Parent 2") := lapply(parents, trimws)]
                     
                     updateDisplayTable()
                     updateTabsetPanel(inputId = "main", selected = "File 1")
                 })
    
    observeEvent(input$addUUIDs,
                 {
                     req(input$fileInput1)
                     values$data1 %>%
                         .[, "gobii.uuid" := uuid::UUIDgenerate(n = .N)]
                     updateDisplayTable()
                     updateTabsetPanel(inputId = "main", selected = "File 1")
                 })
    
    # # login ----
    # validate_password_basic <- eventReactive(
    #     input$ab_login_button_basic,
    #     {
    #         if (input$ti_user_name_basic != user_base_basic_tbl$user_name)
    #             return(FALSE)
    #         else if (input$ti_user_name_basic != user_base_basic_tbl$password)
    #             return(FALSE)
    #         TRUE
    #     }
    # )
    # 
    # observeEvent(eventExpr = validate_password_basic(), 
    #              handlerExpr = shinyjs::hide(id = "login-basic"))
    # 
    # output$display_content_basic <- renderUI({
    #    req(validate_password_basic())
    #    div(class = "bg-success",
    #        id = "success_basic",
    #        h4("Access confirmed!"),
    #        p("Welcome to your basically-secured application!"))
    # })
})
