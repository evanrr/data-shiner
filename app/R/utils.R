library(data.table)

# I/O ----

drop_blank_cols <- function(dt) {
  cols_to_drop = sapply(
    dt, 
    function(x) all(is.na(x))
  ) %>% 
    which() %>% 
    names()
  dt[, (cols_to_drop) := NULL]
  dt[]
}

file_extension <- function(filename) {
  str_split(filename, "\\.") %>%
    unlist() %>%
    last()
}

file_type <- function(filename) {
  require(stringr)
  require(purrr)
  file_extension(filename) %>%
    when(
      . %in% delim ~ 1,
      . %in% excel ~ 2,
      ~ 0,
      delim = c("txt", "tsv", "csv"),
      excel = c("xlsx", "xls")
    )
}

read_samples_excel <- function(filename, sheetname, header.row, ...) {
  read_excel(filename,
             sheet = sheetname,
             skip = header.row,
             col_types = "text",
             # .name_repair = "universal",
             trim_ws = TRUE,
             ...
  ) %>%
    as.data.table() %>%
    .[, seq(1, min(length(.), 14)), with = FALSE] %>%
    drop_blank_cols()
    # setnames(names(.), gsub("(^[^A-Z]+|\\*$)", "", names(.)))
}

read_samples_delim <- function(filename, header.row) {
  require(readr)
  
  message("Reading file '", filename, "'")
  
  encoding = ifelse(
    guess_encoding(filename)$encoding[1] == "UTF-8",
    "UTF-8",
    "Latin-1"
  )
  
  message("\tEncoding: ", encoding)
  
  fread(file = filename,
        encoding = encoding,
        header = header.row > 0,
        skip = header.row - 1,
        fill = TRUE,
        strip.white = TRUE, 
        stringsAsFactors = FALSE
  ) %>%
    drop_blank_cols()
}

downloadDelim <- function(filename, data, csv = FALSE) {
  extension <- ifelse(csv, ".csv", ".txt")
  sep <- ifelse(csv, ",", "\t")
  downloadHandler(
    filename = sub(
      filename,
      pattern = "\\.[^.]+$", 
      replacement = paste0(".cleaned", extension)
    ),
    content = function(file) {
      temp.file <- tempfile(fileext = extension)
      fwrite(x = data,
             file = temp.file, 
             sep = sep, 
             quote = FALSE, 
             row.names = FALSE, 
             col.names = TRUE)
      system2(command = "iconv", 
              args = c("-f", "UTF-8",
                       "-t", "ISO8859-1",
                       "-o", file,
                       temp.file))
      file.remove(temp.file)
    }
  )
}

# UI ----

file_input_panel <- function(
  title = "",
  number = 1
) {
  
  inputs <- list(
    name = paste0("uploadFilePanel", number),
    file = paste0("fileInput", number),
    selectSheet = paste0("selectSheet", number),
    headerRow = paste0("headerRow", number),
    viewData = paste0("viewData", number)
  )
  
  outputs <- list(
    excelFlag = paste0("excelFlag", number),
    data = paste0("data", number)
  )
  
  tags$div(
    # tags$br(),
    fluidRow(
      column(width = 12,
             tags$b(title))
    ),
    
    fluidRow(column(
      width = 12,
      fileInput(
        inputId = inputs$file,
        label = NULL,
        multiple = FALSE,
        accept = c(".txt", ".tsv", "text/tab-separated-values", ".csv", ".xlsx", ".xls"), 
      )
    )),
    
    fluidRow(
      column(
        width = 6,
        conditionalPanel(condition = paste0("output.", outputs$excelFlag),
                         uiOutput(inputs$selectSheet))
      ),
      column(
        width = 3,
        # align = "left",
        numericInput(inputId = inputs$headerRow,
                     label = "Header row",
                     value = 0,
                     min = 0,
                     step = 1)
      ),
      column(
        width = 3,
        align = "center",
        style = "margin-top: 2%",
        actionButton(
          inputId = inputs$viewData,
          label = tags$b("View"),
          icon = icon("eye")
        )
      )
    )
  )
}

# server ----

validate_germplasm <- function(samples, synonyms) {
  require(stringdist)
  clean_sample_vector <- function(x) x %>% tolower() %>% gsub("\\s+", "", .)
  samples.clean <- samples %>% clean() %>% unique()
  synonyms.clean <- synonyms[, sapply(.SD, clean_sample_vector)]
  
}
