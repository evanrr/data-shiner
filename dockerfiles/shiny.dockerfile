FROM rocker/shiny-verse:4.1.0

RUN install2.r --error DT data.table stringdist shinyBS

EXPOSE 3838

COPY shiny-server.sh /usr/bin/shiny-server.sh

COPY app /srv/shiny-server/.

CMD ["/usr/bin/shiny-server.sh"]
