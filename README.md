## Author

Evan R Rees

err87@cornell.edu

August 2021

## Description

A Shiny app for performing simple manipulations of sample metadata. This repository includes a Shiny server for deployment and RStudio server for development.

## Quick Start

To start the Shiny server:

    docker-compose up -d shiny

The app will then be visible at `localhost:3883`.

To stop the server:

    docker-compose down

## Development

Start the server:

    docker-compose up -d rstudio

The app will be visible at `localhost:8787`.

